/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/isn/ci/proto/UWL/reuse/manageEduBiz/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});