sap.ui.define([
	"./BaseController",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel"
], function (BaseController, MessageBox, JSONModel) {
	"use strict";

	return BaseController.extend("com.istn.ci.proto.UWL.reuse.manageEduBiz.controller.PlanEduBizDetail", {


		onInit: function () {
			this._showFragment();
			var data = this.getOwnerComponent().getModel("bizDetail").getData();
			this.getView().setModel(new JSONModel(data), "detail")
		},
		
		_showFragment: function(){
			var EduReadonlySimpleFormTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduReadonlySimpleForm", this);
			var page = this.getView().byId("eduBizDetail");
			if(!EduReadonlySimpleFormTemplate) return MessageBox.error("No template");
			page.insertContent(EduReadonlySimpleFormTemplate);
		},


	});

});