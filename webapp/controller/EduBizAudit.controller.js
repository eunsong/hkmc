sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"../model/formatter"
], function (BaseController, JSONModel, MessageBox, formatter) {
	"use strict";

	return BaseController.extend("com.istn.ci.proto.UWL.reuse.manageEduBiz.controller.EduBizAudit", {
		
		formatter: formatter,
		
		currentPerson : "",
		
		FLOWCODE: "MBIZ",
		
		onInit: function () {
			this._initTemplates();
			this._initModels();
			this.getRouter().getRoute("EduBizAudit").attachMatched(this._onRouteMatchedEduBizAudit, this);
		},
		
		_onRouteMatchedEduBizAudit: function(event){
			var appLayout = {
				header: false,
				requestBtn: { visible: true, enabled: false},
				editBtn: { visible: true, enabled: false},
				rejectBtn: { visible: true, enabled: false},
				approvalBtn: { visible: true, enabled: false},
				createBtn : { visible: false, enabled: false},
				pasteBtn: { visible: false, enabled: false},
				Audit01 : { visible: false },
				Audit02 : { visible: false },
				Audit03 : { visible: false },
				Audit04 : { visible: false },
				Audit05 : { visible: false },
				Status : { visible: true }
			};
			this.getView().getModel("appLayout").setData(appLayout);
			this._showFragment('EduReadonlySimpleForm');
			this._showFragment('EduReadonlyTable');
			this.currentPerson = event.getParameter("arguments").person;
			this._getEduBizPlans();
		},
		
		_getEduBizPlans: function(event){
			var that = this;
			
			$.ajax({
				url: "/hkmc_odata/hkmc/TBL003?$expand=DETAIL",
				method: "GET",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				success: function(data, headers){
					var bizData = data.value.filter(function(data){
						return data.STATUS <= that.currentPerson;
					});
					that.getView().getModel("bizModel").setData(that._getTextByKey(bizData));
				},
				error: function(error){
					debugger
					MessageBox.error(that.i18n.getText("EduBizMain.controller.error"));
				}
			});
		},
		
		//key로 저장된 데이터 text필드 생성
		_getTextByKey: function(data){
			for(var index=0; index<=data.length-1; index++){
				//교육 일수
				var daysOfCourse = data[index].FILED08.split(".")
				data[index].FILED08 = daysOfCourse[0];
				data[index].FILED080 = daysOfCourse[1];
				//기업명
				if(data[index].FILED02 === "100"){ data[index].FILED02_text = "현대자동차"; }else{ data[index].FILED02_text = "기아자동차"; }
				//교육목적
				if(data[index].FILED04 === "in"){ data[index].FILED04_text = "사내교육"; }else{ data[index].FILED04_text = "사외교육"; }
				//교육형태
				if(data[index].FILED05 === "off"){ data[index].FILED05_text = "집합"; }else{ data[index].FILED05_text = "이러닝"; }
				//교육대상
				if(data[index].FILED07 === "normal"){ data[index].FILED07_text = "일반"; }else{ data[index].FILED07_text = "임원"; }
				//교육주관
				if(data[index].FILED13 === "1"){ data[index].FILED13_text = "인재개발원"; }else{ data[index].FILED13_text = "미래설계원"; }
				//카테고리구분
				if(data[index].FILED17 === "common"){ data[index].FILED17_text = "공통"; }else{ data[index].FILED17_text = "직무"; }
			}
			return data;	
		},
		
		_initModels: function(event){
			// var appLayout = {
			// 	header: false,
			// 	requestBtn: { visible: true, enabled: false},
			// 	editBtn: { visible: true, enabled: false},
			// 	rejectBtn: { visible: true, enabled: false},
			// 	approvalBtn: { visible: true, enabled: false},
			// 	createBtn : { visible: false, enabled: false},
			// 	pasteBtn: { visible: false, enabled: false},
			// 	Audit01 : { visible: false },
			// 	Audit02 : { visible: false },
			// 	Audit03 : { visible: false },
			// 	Audit04 : { visible: false },
			// 	Audit05 : { visible: false },
			// 	Status : { visible: true }
			// };
			// // this.getView().setModel(new JSONModel(appLayout), "appLayout");
			this.getView().setModel(new JSONModel({}), "appLayout");
			this.getView().setModel(new JSONModel({}), "bizModel");
			this.getView().setModel(new JSONModel({}), "detail");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		
		_initTemplates: function(){
			this.EduReadonlyTableTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduReadonlyTable", this);
			this.EduReadonlySimpleFormTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduReadonlySimpleForm", this);
			this.EduEditableSimpleFormTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduEditableSimpleForm", this);
			// this.EduAuditSimpleFormTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduAuditSimpleForm", this);
		},
		
		_showFragment: function(mode){
			var page = this.getView().byId("eduBizAudit");
			var template = this[`${mode}Template`];
			if(!template) return MessageBox.error("No template");
			page.insertContent(template);
		},
		
		onSelectList: function(event){
			var bizData = this.getView().getModel("bizModel").getData();
			var appLayout = this.getView().getModel("appLayout");
			var index = event.getSource().getSelectedContextPaths()[0].split("/").reverse()[0];
			
			if(bizData[index].STATUS == parseInt(this.currentPerson) - 2){	//승인건
				appLayout.setProperty("/requestBtn/enabled", false);
				appLayout.setProperty("/approvalBtn/enabled", true);
				appLayout.setProperty("/editBtn/enabled", false);
			}else if(bizData[index].STATUS == parseInt(this.currentPerson) - 1){	//결재건(상신)
				appLayout.setProperty("/requestBtn/enabled", true);
				appLayout.setProperty("/approvalBtn/enabled", false);
				appLayout.setProperty("/editBtn/enabled", true);
			}
		},
		
		onList: function(event){
			var index = event.getSource().getBindingContextPath().split("/").reverse()[0];
			var bizData = this.getView().getModel("bizModel").getData();
			this.getView().getModel("detail").setData(bizData[index]);
		},
		
		onRequest: function(event){
			var that = this;
			var selectedIdx = this.getView().byId("readonly_eduBiz").getSelectedContextPaths()[0].split("/").reverse()[0];
			var data = this.getView().getModel("bizModel").getData()[selectedIdx];
			MessageBox.confirm(this.i18n.getText("EduBizAudit.controller.Approval"),{
				onClose: function (oAction) {
					if(oAction === "OK"){
						that._reorganizeData(data);
					}
				}
			});     
		},
		
		onEdit: function(event){
			
		},
		
		_reorganizeData: function(data){
			var today = new Date();
			data.FLOWCODE = this.FLOWCODE;
			debugger
			data.STATUS = this.currentPerson;
			data.CREATE_DATE = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); //상신일자
			data.CREATE_TIME = today.toLocaleTimeString('it-IT');  //상신시간
			
			this._updateBiz(JSON.parse(JSON.Stringify(data)));
		},
		
		
		_updateBiz: function(data){
			debugger
			
		},
		
		

	});

});