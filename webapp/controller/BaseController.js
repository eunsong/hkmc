sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel"
], function(Controller, History, UIComponent, JSONModel) {
	"use strict";

	return Controller.extend("com.istn.ci.proto.UWL.reuse.manageEduBiz.controller.BaseController", {
		onInit: function () {

		},
		
		getRouter: function(){
			return UIComponent.getRouterFor(this);
		},
		
		onNavBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("EduBizMain", {}, true);
			}
		},
		
	});
});