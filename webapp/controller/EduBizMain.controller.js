sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	'sap/ui/model/Filter',
	"../model/formatter"
], function (BaseController, JSONModel, MessageBox, Filter, formatter) {
	"use strict";

	return BaseController.extend("com.istn.ci.proto.UWL.reuse.manageEduBiz.controller.EduBizMain", {
		
		formatter: formatter,
		globalConfig: {
			company : [
				{text : "현대자동차", key : 100 },
				{text : "기아자동차", key : 200 }
			],
			purposeOfCourse : [
				{text : "사내교육", key : "in" },
				{text : "사외교육", key : "out" }
			],
			typeOfCourse: [
				{text: "집합", key: "off"},	
				{text: "이러닝", key: "on"}	
			],
			requiredOfCourse: [
				{text: "법정 필수", key: "Y"},	
				{text: "법정 자격", key: "N"}	
			],
			targetOfCourse: [
				{text: "일반", key: "normal"},	
				{text: "임원", key: "executive"}	
			],
			daysOfCourse: [
				{text: "0", key: "0"},	
				{text: "1", key: "1"},	
				{text: "2", key: "2"},	
				{text: "3", key: "3"},	
				{text: "4", key: "4"},	
				{text: "5", key: "5"},	
				{text: "6", key: "6"},
				{text: "7", key: "7"},	
				{text: "8", key: "8"},	
				{text: "9", key: "9"}	
			],
			managementOfCourse: [
				{text: "인재개발원", key: "1"},	
				{text: "미래설계원", key: "2"}	
			],
			insuranceOfCourse: [
				{text: "YES", key: "Y"},	
				{text: "NO", key: "N"}	
			],
			intranetOfCourse: [
				{text: "YES", key: "Y"},	
				{text: "NO", key: "N"}	
			],
			categoryOfCourse: [
				{text: "공통", key: "common"},	
				{text: "직무", key: "duty"}	
			],
			currentPerson: [
				{text: "물류운영팀-강동원", key: "01"},
				{text: "물류운영팀 팀장-신혜성", key: "03"},
				{text: "경영지원팀 담당자-임정희", key: "05"},
				{text: "경영지원팀 팀장-아이유", key: "07"},
				{text: "인재개발팀 담당자-닉쿤", key: "09"},
				{text: "인재개발팀 팀장-심혜진", key: "11"}
			]
		},
		
		FLOWCODE: "MBIZ",
		
		onInit: function () {
			this._initModels();
			this._initTemplates();
			this._showFragment('EduReadonlyTable');
			this._getEduBizPlans();
			this.getRouter().getRoute("EduBizMain").attachMatched(this._onRouteMatchedEduBizMain, this);
		},
		
		_onRouteMatchedEduBizMain: function(){
			var appLayout = {
				header: true,
				requestBtn: { visible: false, enabled: false },
				rejectBtn: { visible: false, enabled: false },
				editBtn: { visible: false, enabled: false },
				approvalBtn: { visible: false, enabled: false },
				createBtn : { visible: true, enabled: false },
				pasteBtn: { visible: true, enabled: false },
				Audit01 : { visible: false },
				Audit02 : { visible: false },
				Audit03 : { visible: false },
				Audit04 : { visible: false },
				Audit05 : { visible: false },
				Status : { visible: false }
			};
			this.getView().getModel("appLayout").setData(appLayout);
			this.getView().byId("selectPerson").setSelectedKey("0");
		},
		
		_initModels: function(event){
			this.getView().setModel(new JSONModel({}), "appLayout");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this.getView().setModel(new JSONModel(this.globalConfig), "globalConfig");
			this.getView().setModel(new JSONModel({}), "newEduBiz");
			this.getView().setModel(new JSONModel({}), "newBizLayout");
		},
		
		_getEduBizPlans: function(event){
			var that = this;
			
			$.ajax({
				url: "/hkmc_odata/hkmc/TBL003",
				method: "GET",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				success: function(data, headers){
					var nData = that._getTextByKey(data.value);
					if(that.getView().getModel("bizModel")){
						that.getView().getModel("bizModel").setData(nData); 
					}else{
						that.getView().setModel(new JSONModel(nData), "bizModel");
					}
				},
				error: function(error){
					debugger
					MessageBox.error(that.i18n.getText("EduBizMain.controller.error"));
				}
			});
		},
		
		_initTemplates: function(event){
			this.EduReadonlyTableTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduReadonlyTable", this);
			this.EduEditableSimpleFormTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.proto.UWL.reuse.manageEduBiz.view.EduEditableSimpleForm", this);
		},
		
		_showFragment: function(mode){
			var appLayoutModel = this.getView().getModel("appLayout");
			var page = this.getView().byId("eduBizMain");
			var template = this[`${mode}Template`];
			
			if(mode === 'EduReadonlyTable'){
				appLayoutModel.setProperty("/header", true);
				appLayoutModel.setProperty("/pasteBtn/visible", true);
				appLayoutModel.setProperty("/pasteBtn/enabled", false);
			}else{
				appLayoutModel.setProperty("/header", false);
			}
			
			if(!template) return MessageBox.error("No template");
			page.setContent(template);
		},
		
		//key로 저장된 데이터 text필드 생성
		_getTextByKey: function(data){
			for(var index=0; index<=data.length-1; index++){
				//교육 일수
				var daysOfCourse = data[index].FILED08.split(".")
				data[index].FILED08 = daysOfCourse[0];
				data[index].FILED080 = daysOfCourse[1];
				//기업명
				if(data[index].FILED02 === "100"){ data[index].FILED02_text = "현대자동차"; }else{ data[index].FILED02_text = "기아자동차"; }
				//교육목적
				if(data[index].FILED04 === "in"){ data[index].FILED04_text = "사내교육"; }else{ data[index].FILED04_text = "사외교육"; }
				//교육형태
				if(data[index].FILED05 === "off"){ data[index].FILED05_text = "집합"; }else{ data[index].FILED05_text = "이러닝"; }
				//교육대상
				if(data[index].FILED07 === "normal"){ data[index].FILED07_text = "일반"; }else{ data[index].FILED07_text = "임원"; }
				//교육주관
				if(data[index].FILED13 === "1"){ data[index].FILED13_text = "인재개발원"; }else{ data[index].FILED13_text = "미래설계원"; }
				//카테고리구분
				if(data[index].FILED17 === "common"){ data[index].FILED17_text = "공통"; }else{ data[index].FILED17_text = "직무"; }
			}
			return data;	
		},
		
		onChangePerson: function(event){
			var key = event.getParameter("selectedItem").getKey();
			if(key != "00"){
				this.getRouter().navTo("EduBizAudit", { person : key });
			}
		},

		/*
		년도 필터링
		*/
		onSearch: function(event){
			var filters = [];
			var table = this.getView().byId("readonly_eduBiz");

			event.getParameter("selectionSet").forEach(item=>{
				if(item.getValue()){ 
					filters.push(new Filter("FILED01", sap.ui.model.FilterOperator.EQ, item.getValue()) );
				};
			});
            table.getBinding("items").filter(filters);
		},
		onList: function(event){
			var selectedRow = event.getSource().getBindingContextPath().split("/").reverse()[0];
			var selectedData = this.getView().getModel("bizModel").getData()[selectedRow];
			
			this.getOwnerComponent().setModel(new JSONModel(selectedData), "bizDetail");
			this.getRouter().navTo("EduBizDetail")
		},

		onSelectList: function(event){
			this.getView().getModel("appLayout").setProperty("/pasteBtn/enabled", true);
			var selectedRow = event.getParameter("listItem").getBindingContextPath().split("/").reverse()[0];
			var selectedData = this.getView().getModel("bizModel").getData()[selectedRow];

			var createModel = this.getView().getModel("newEduBiz");
			createModel.setData(selectedData);
		},
		
		onCreate: function(event){
			var bizData = {
				planedYearValueSate : "None",
				companyValueSate : "None",
				nameOfCourseValueSate : "None",
				nightsOfCourseValueSate : "None",
				daysOfCourseValueSate : "None",
			};
			
			this.getView().getModel("newBizLayout").setData(bizData);
			this._showFragment('EduEditableSimpleForm');
			
			if(event.getSource().getId().indexOf("create") != -1){
				this.getView().getModel("newEduBiz").setData({});
			}
		},
		
		/*
			simpleForm	
		*/
		
		// //초기 valueState None으로 setting
		// _setValueStateNone: function(event){
		// 	var elements = document.getElementsByClassName("allFields") ;
			
		// 	Object.keys(elements).forEach(function(key) {
		// 		var tmpElement = elements[key];
		// 		var realElement = sap.ui.getCore().byId(tmpElement.getAttribute('id'));
		// 		realElement.setValueState(sap.ui.core.ValueState.None);
		// 	});
		// },
		
		onChangePlanedYear: function(event){
			var currentYear = new Date().getFullYear(); 
			var value = event.getParameter("value");
			if(isNaN(value)){
				event.getSource().setValueState("Error");
				return MessageBox.warning(this.i18n.getText("EduBizMain.controller.PlanedYearFormat"));
			}else if(value < currentYear) {
				event.getSource().setValueState("Error");
				return MessageBox.warning(this.i18n.getText("EduBizMain.controller.PlanedYearMsg"));
			}else{
				event.getSource().setValueState("None");
			}
		},
		
		onChangeMandaFields: function(event){
			if(event.getParameter("selectedItem")){	//회사 구분
				event.getSource().setValueState("None");
			}else if(event.getParameter("value").length > 0){	//교육과정명
				event.getSource().setValueState("None");
			}else{
				event.getSource().setValueState("Error");
				MessageBox.warning(this.i18n.getText("EduBizMain.controller.MandatoryFields"));
			};
		},
		
		/*
		// 교육 일수 (n박 m일)
		*/
		onChangeDaysOfCourse: function(event){
			var bizData = this.getView().getModel("newEduBiz").getData();
			if(bizData.FILED08 >= bizData.FILED080){
				event.getSource().setSelectedItem(null);
				event.getSource().setValueState("Error");
				MessageBox.warning(this.i18n.getText("EduBizMain.controller.ValidDays"))
			}else{
				event.getSource().setValueState("None");
			}
		},
		
		onSave: function(event){
			var that = this;
			var newEduBizModel = this.getView().getModel("newEduBiz");
			var newBiz = JSON.parse(JSON.stringify(newEduBizModel.getData()));
			
			if(this._checkMandatoryFields()){
				MessageBox.confirm(this.i18n.getText("EduBizMain.controller.Save"),{
					onClose: function (oAction) {
						if(oAction === "OK"){
							that._reorganizeData(newBiz);
						}
					}
				});                   
			};
		},
		
		onCancel: function(event){
			var that = this; 
			
			if(this._isDataChanged()){
				MessageBox.confirm(this.i18n.getText("EduBizMain.controller.Cancel"),{
					onClose: function (oAction) {
						if(oAction === "OK"){
							that._showFragment('EduReadonlyTable');
						};
					}
				}); 
			}else{
				this._showFragment('EduReadonlyTable');
			}
		},
		
		_isDataChanged: function(event){
			var that = this;
			var data = this.getView().getModel("newEduBiz").getData();

			for(var key in data){
				if(data[`${key}`] === ""){
					// 데이터 값 없는 경우
					data[`${key}`] = null;	
				}else{
					//데이터 있는경우
					var newData = data[`${key}`];
				}
				if(newData) return true;
			}
			//어느 데이터도 바꾸지 않은 경우
			return false;
		},
		
		_checkMandatoryFields: function(event){
			//필수 입력값 체크
			var chkMandResult = true;
			var chkMandCnt = 0;
			var chkInvalidInputCnt = 0;
			var elements = document.getElementsByClassName("allFields") ;
				
			Object.keys(elements).forEach(function(key) {
				var tmpElement = elements[key];
				var realElement = sap.ui.getCore().byId(tmpElement.getAttribute('id'));
				var type = realElement.getMetadata().getElementName().split(".").reverse()[0];
				
				switch(type){
					case "Input" :
						if(realElement.getValue() === "" && realElement.hasStyleClass("mandatoryFields")){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							//유효하지 않는 기존 입력값이 있는 경우 저장X
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
						
					case "Select":
						if(realElement.getSelectedKey() === "" && realElement.hasStyleClass("mandatoryFields")){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
				}
			});
		 
			if(chkInvalidInputCnt > 0){
				chkMandResult = false;
				MessageBox.error(this.i18n.getText("EduBizMain.controller.checkValidInput"));
			}else if(chkMandCnt > 0){
				chkMandResult = false;
				MessageBox.error(this.i18n.getText("EduBizMain.controller.checkAllMandatoryFields"));
			 }
			 return chkMandResult;
			
		},
		
		
		_reorganizeData: function(newBiz){
			var today = new Date();
			newBiz.FLOWCODE = this.FLOWCODE;
			newBiz.STATUS = "01";
			newBiz.CREATE_DATE = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); //상신일자
			newBiz.CREATE_TIME = today.toLocaleTimeString('it-IT');  //상신시간
	        newBiz.CREATOR = "00734725"; //상신자
	        newBiz.CREATOR_NAME = "강동원" //상신자명
	        newBiz.CREATOR_ORGEH = "H0122120" ; //조직
	        newBiz.CREATOR_ORGTX = "물류운영팀"; //조직명
	        newBiz.FILED08 = newBiz.FILED08 + "." + newBiz.FILED080;	//교육일수
	        delete newBiz.FILED080;
	        delete newBiz.FLOWUUID;	//기존 사업 복사경우 ID 삭제
	        delete newBiz.FILED02_text;
	        delete newBiz.FILED04_text;
	        delete newBiz.FILED05_text;
	        delete newBiz.FILED07_text;
	        delete newBiz.FILED13_text;
	        delete newBiz.FILED17_text;
	        newBiz.DETAIL= [
		        {
		            "NO1": "01",
		            "STATUS" : "01",
		            "START_DATE" : newBiz.CREATE_DATE,
		            "START_TIME" : newBiz.CREATE_TIME,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "00722225",
		            "APPROVAL_NAME" : "신혜성",
		            "APPROVAL_ORGEH" : "H0122120",
		            "APPROVAL_ORGTX" : "물류운영팀",
		            "APPR_STATUS" : "P"
		        },
		        {
		            "NO1": "02",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "00722225",
		            "APPROVAL_NAME" : "신혜성",
		            "APPROVAL_ORGEH" : "H0122120",
		            "APPROVAL_ORGTX" : "물류운영팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "03",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "02117388",
		            "APPROVAL_NAME" : "임정희",
		            "APPROVAL_ORGEH" : "K0151110",
		            "APPROVAL_ORGTX" : "경영지원팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "04",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "02117388",
		            "APPROVAL_NAME" : "임정희",
		            "APPROVAL_ORGEH" : "K0151110",
		            "APPROVAL_ORGTX" : "경영지원팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "05",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "02116272",
		            "APPROVAL_NAME" : "아이유",
		            "APPROVAL_ORGEH" : "K0151110",
		            "APPROVAL_ORGTX" : "경영지원팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "06",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "02116272",
		            "APPROVAL_NAME" : "아이유",
		            "APPROVAL_ORGEH" : "K0151110",
		            "APPROVAL_ORGTX" : "경영지원팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "07",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "00727182",
		            "APPROVAL_NAME" : "닉쿤",
		            "APPROVAL_ORGEH" : "H0110120",
		            "APPROVAL_ORGTX" : "인재개발팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "08",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "00727182",
		            "APPROVAL_NAME" : "닉쿤",
		            "APPROVAL_ORGEH" : "H0110120",
		            "APPROVAL_ORGTX" : "인재개발팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "09",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "00718525",
		            "APPROVAL_NAME" : "심혜진",
		            "APPROVAL_ORGEH" : "H0110120",
		            "APPROVAL_ORGTX" : "인재개발팀",
		            "APPR_STATUS" : null
		        },
		        {
		            "NO1": "10",
		            "STATUS" : "01",
		            "START_DATE" : null,
		            "START_TIME" : null,
		            "END_DATE" : null,
		            "END_TIME" : null,
		            "CREATOR" : "00718525",
		            "APPROVAL_NAME" : "심혜진",
		            "APPROVAL_ORGEH" : "H0110120",
		            "APPROVAL_ORGTX" : "인재개발팀",
		            "APPR_STATUS" : null
		        }
		    ]
			this.createEduBizPlan(newBiz);
		},
		
		createEduBizPlan: function(data){
			var that = this;
			var page = this.getView().byId("eduBizMain");
			
			page.setBusy(true);
			$.ajax({
				url: "/hkmc_odata/hkmc/TBL003",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data: JSON.stringify(data),
				method: "POST",
				success: function(data, headers){
					that._getEduBizPlans();
					MessageBox.success(that.i18n.getText("EduBizMain.controller.successSave"), {
						onClose: function (oAction) { 
							if(oAction === "OK"){
								that._showFragment('EduReadonlyTable');
								that.getView().getModel("newBizModel").setData({});
							}
						}
					});
				},
				error: function(error){
					MessageBox.error(that.i18n.getText("EduBizMain.controller.error"));
				},
				complete: function(){
					page.setBusy(false);
				}
			});
		}
	});

});